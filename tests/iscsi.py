#!/usr/bin/python

# Copyright (C) 2016 Red Hat, Inc.
# This file is part of libsan.
#
# libsan is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# libsan is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with libsan.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import absolute_import, division, print_function, unicode_literals
import libsan.host.iscsi as iscsi
import libsan.host.lio as lio
import libsan.host.linux as linux

target = "localhost"


def _clean_up():
    if not iscsi.clean_up():
        print("FAIL: Could not clean up iSCSI")
        return False
    return True


def _remove_target():
    if not lio.lio_clearconfig():
        print("FAIL: Could not clean up LIO target")
        return False
    return True


def test_install_initiator():
    if not iscsi.install():
        print("FAIL: Could not install iSCSI initiator package")
        assert 0
    assert 1


def test_discovery():
    if linux.is_docker():
        print("SKIP: Test does not run on container...")
        return

    if not lio.lio_setup_iscsi_target():
        print("FAIL: Could not create iSCSI target")
        assert 0

    if not iscsi.discovery_st(target):
        print("FAIL: Could not discovery iSCSI target")
        _clean_up()
        _remove_target()
        assert 0

    _clean_up()
    _remove_target()
    assert 1
